from django import forms
from .models import Tipo, Planta

class TipoForm(forms.ModelForm):
    class Meta:
        model = Tipo
        fields = ['nombre', 'cantidad', 'tamaño']

        labels = {
            'nombre': 'Nombre',
            'cantidad': 'Cantidad',
            'tamaño': 'Tamaño',

        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'cantidad': forms.TextInput(attrs={'class': 'form-control'}),
            'tamaño': forms.TextInput(attrs={'class': 'form-control'}),
           
        }
