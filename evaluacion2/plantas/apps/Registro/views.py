from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from .models import Tipo, Planta
from .forms import TipoForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.db.models import Q 

def listar_tipos(request):
    tipos = Tipo.objects.all()
    return render(request, "Registro/listar_tipos.html", {'tipos': tipos})

def agregar_tipo(request):
    if request.method == "POST":
        form = TipoForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.save()
            return redirect("/")
    else:
        form = TipoForm()
        return render(request, "Registro/agregar_tipo.html", {'form': form})

def borrar_tipo(request, tipo_id):
    instancia = Tipo.objects.get(id=tipo_id)
    instancia.delete()

    return redirect('listar_tipos')

def editar_tipo(request, tipo_id):
    instancia = Tipo.objects.get(id=tipo_id)

    form = TipoForm(instance=instancia)

    if request.method == "POST":
        form = TipoForm(request.POST, instance=instancia)
        if form.is_valid():
            instancia = form.save(commit=False)
            instancia.save()

    return render(request, "Registro/editar_tipo.html", {'form': form})

class TipoCreate(CreateView):
    model = Tipo
    form_class = TipoForm
    template_name = 'Registro/tipo_crear.html'
    success_url = reverse_lazy("listar_tipos")

class TipoList(ListView):
    model = Tipo
    template_name = 'Registro/list_tipos.html'

class TipoUpdate(UpdateView):
    model = Tipo
    form_class = TipoForm
    template_name = 'Registro/tipo_crear.html'
    success_url = reverse_lazy('list_tipos')

        

class TipoDelete(DeleteView):
    model = Tipo
    template_name = 'Registro/tipo_delete.html'
    success_url = reverse_lazy('list_tipos')
 

    template_name = 'Registro/tipo_delete.html'
    success_url = reverse_lazy("list_tipos")


class BuscarPlantasView(ListView):
    model = Planta
    template_name = 'Registro/buscar_plantas.html'


class SearchResultsView(ListView):
    model =Planta
    template_name = 'Registro/search_results.html'
    
    def get_queryset(self): 
        query = self.request.GET.get('q')
        object_list = Planta.objects.filter(
            Q(tamaño__icontains=query) )
        
        return object_list

def mantenedor(request):
    lista= Tipo.objects.all()
    cant_cantidad= request.GET.get('cant-cantidad')
    nombre_tipo= request.GET.get('nombre-tipo')

    if 'btn-buscarCantcantidad' in request.GET:
       if cant_cantidad: 
           lista= Tipo.objects.filter(cantidad__gte=cant_cantidad)
    elif 'btn-nombre-tipo' in request.GET:
        if nombre_tipo:
            lista= Tipo.objects.filter(nombre__icontains=nombre_tipo)
      
    data = {
        'object_list': lista
    }
    return render(request, 'Registro/list_tipos_filtros.html', data)

