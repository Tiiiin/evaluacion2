from django.test import TestCase
from apps.Registro.models import Tipo
from apps.Registro.forms import TipoForm

class TipoFormCase(TestCase):
        
    def test_valid_form(self):
        tipo = Tipo.objects.create(nombre='Mandragora',cantidad=10)
        data = {'nombre': tipo.nombre, 'cantidad': tipo.cantidad, }
        form = TipoForm(data=data)
        self.assertTrue(form.is_valid())
        
    def test_invalid_form(self):
        tipo = Tipo.objects.create(nombre='',cantidad=10)
        data = {'nombre': tipo.nombre, 'cantidad': tipo.cantidad, }
        form = TipoForm(data=data)
        self.assertFalse(form.is_valid())
