from django.test import TestCase
from django.template.defaultfilters import slugify
from apps.Registro.models import Tipo

class TipoTestCase(TestCase):
    def setUp(self):
        Tipo.objects.create(nombre="bull",cantidad=4)
        Tipo.objects.create(nombre="ivy",cantidad=6)

    def test_ingresar_tipos(self):
        """Los tipos se registran correctamente en la BD"""
        tipo_1 = Tipo.objects.get(nombre="bull")
        tipo_2 = Tipo.objects.get(nombre="ivy")
        self.assertEqual(tipo_1.cantidad, 4)
        self.assertEqual(tipo_2.cantidad, 6)
