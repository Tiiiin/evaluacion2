from django.db import models

# Create your models here.
class Tipo(models.Model):
    nombre = models.CharField(max_length=20)
    cantidad = models.IntegerField()
    tamaño = models.CharField(max_length=10)
    def __str__(self):
        return self.nombre

class Planta(models.Model):
    tipo = models.ForeignKey(Tipo, null=True, blank=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=15)
    descripcion = models.CharField(max_length=50)
    fecha_compra = models.DateTimeField(auto_now=True)
    precio = models.IntegerField()
    disponibles = models.IntegerField()
    tamaño = models.CharField(max_length=10)


    def __str__(self):
        return self.nombre
