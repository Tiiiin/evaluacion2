from django.contrib import admin
from .models import Planta, Tipo

# Register your models here.
admin.site.register(Tipo)
admin.site.register(Planta)
