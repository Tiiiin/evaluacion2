from django.urls import path, include
from . import views
from django.contrib.auth.views import login_required
from .views import SearchResultsView, BuscarPlantasView
urlpatterns = [

    path('listarTipos', views.listar_tipos, name="listar_tipos"),

    path('agregar_tipo', views.agregar_tipo, name="agregar_tipo"),

    path('editar_tipo/<int:tipo_id>', login_required(views.editar_tipo),name="editar_tipo"),

    path('borrar_tipo/<int:tipo_id>', login_required(views.borrar_tipo), name="borrar_tipo"),

    path('add_tipo', views.TipoCreate.as_view(), name="add_tipo"),

    path('add_tipo', views.TipoCreate.as_view(), name="add_tipo"),

    path('list_tipos/', views.TipoList.as_view(), name='list_tipos'),

    path('edit_tipo/<int:pk>', views.TipoUpdate.as_view(), name='edit_tipo'),

    path('del_tipo/<int:pk>', views.TipoDelete.as_view(), name='del_tipo'),

    path('buscar/', BuscarPlantasView.as_view(), name='buscar_plantas'),

    path('search/', SearchResultsView.as_view(), name='search_results'),

    path('mantenedor/', views.mantenedor , name="mantenedor"),  
]




