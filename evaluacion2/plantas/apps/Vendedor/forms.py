from .models import Vendedor
from django import forms

class VendedorForm(forms.ModelForm):
    

    class Meta:
        model = Vendedor
        fields = (
            'fotografia',
            'rut',
            'nombre',
            'email',
            'grado'
        )
        labels = {
            'fotografia':'Fotografia',
            'rut':'RUN',
            'nombre':'Nombre',
            'email':'Correo electronico',
            'grado':'Grado'
        }
        widgets = {
            # 'fotografia':forms.FileInput(attrs={'class':'form-control','type':'file'}),
            'rut':forms.TextInput(attrs={'class':'form-control'}),
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'email':forms.TextInput(attrs={'class':'form-control'}),
            'grado':forms.Select(choices="GRADOS", attrs={'class':'form-control'}),
        }

    
