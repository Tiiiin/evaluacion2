# Generated by Django 3.1.2 on 2020-11-09 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Vendedor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fotografia', models.ImageField(upload_to='vendedores')),
                ('rut', models.CharField(max_length=15)),
                ('nombre', models.CharField(max_length=50)),
                ('email', models.EmailField(blank=True, max_length=70, null=True, unique=True)),
                ('grado', models.CharField(choices=[('Ayudante', 'Ayudante'), ('Vendedor', 'Vendedor'), ('Encargado', 'Encargado'), ('Especialista', 'Especialista')], max_length=50)),
            ],
        ),
    ]
