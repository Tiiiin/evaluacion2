from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from .views import VendedorList, VendedorCreate, VendedorUpdate , VendedorDelete

urlpatterns = [
    path('listar/', VendedorList.as_view(), name="vendedores_list"),
    path('crear/', VendedorCreate.as_view(), name="vendedores_form"),
    path('editar/<int:pk>', VendedorUpdate.as_view(), name="vendedores_update"),
    path('borrar/<int:pk>', VendedorDelete.as_view(), name="vendedores_borrar"),
    
]
