from django.db import models

GRADOS = (
    ('Ayudante', 'Ayudante'),
    ('Vendedor', 'Vendedor'),
    ('Encargado', 'Encargado'),
    ('Especialista', 'Especialista'),
)

class Vendedor (models.Model):
    fotografia = models.ImageField(upload_to='vendedores')
    rut = models.CharField(max_length=15)
    nombre = models.CharField(max_length=50)
    email = models.EmailField(max_length=70,blank=True, null= True, unique= True)
    grado = models.CharField(max_length=50, choices=GRADOS)

    def __str__(self):
        return str(self.fotografia)
